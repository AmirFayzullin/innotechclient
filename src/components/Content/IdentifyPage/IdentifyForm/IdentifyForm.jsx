import React from 'react';
import s from './IdentifyForm.module.css';

class IdentifyForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filePath: undefined,
            imageBlob: undefined
        };

        this.onChange = this.onChange.bind(this);
        this.onSend = this.onSend.bind(this);
    }

    onChange(e) {
        let input = e.target;

        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = ((e) => {
                this.setState({
                    filePath: e.target.result,
                    imageBlob: input.files[0]
                });
            });

            reader.readAsDataURL(input.files[0]);
        }
    }

    onSend(e) {
        e.preventDefault();

        if (this.state.imageBlob === undefined) return;

        let formData = new FormData();

        formData.append("photo_raw", this.state.imageBlob, this.state.imageBlob.name);

        this.props.sendImage(formData);
    }

    render() {
        return (
            <div className={s.identifyFormWrapper}>
                <img className={s.image}
                     src={this.state.filePath || `${process.env.PUBLIC_URL}/empty.svg`}
                     alt="img"
                />

                <form className={s.form}>
                    <label className={s.formItem + " " + s.active}>
                        <p>Выбрать фото</p>
                        <input onChange={this.onChange} type="file"/>
                    </label>
                    <label className={`${s.formItem} ${this.state.imageBlob !== undefined ? s.active : s.inactive}`}>
                        <button className={`${s.sendButton} ${this.state.imageBlob !== undefined ? s.active : s.inactive}`}
                                onClick={this.onSend}>
                            Искать
                        </button>
                    </label>
                </form>
            </div>
        )
    }
}

export default IdentifyForm;