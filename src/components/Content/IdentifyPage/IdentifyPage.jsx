import React from 'react';
import IdentifyForm from "./IdentifyForm/IdentifyForm";
import s from './IdentifyPage.module.css';
import Preloader from "../../Preloader/Preloader";
import Result from "./Result/Result";
import {URLReqConfig} from "../../../URLReqConfig";

class IdentifyPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            resultsReceived: false,
            fetched: false,
            findingResult: undefined
        };

        this.sendImage = this.sendImage.bind(this);
    }

    async sendImage(formData) {
        this.setState({
            isFetching: true,
            resultsReceived: false,
            fetched: true
        });

        let resp;

        fetch(`${URLReqConfig}/find_by_photo/`, {
            method: 'POST',
            body: formData
        })
        .then(async response => {
            resp = await response.json();

            this.setState({
                isFetching: false,
                resultsReceived: resp.items.length !== 0,
                findingResult: resp.items
            });

            console.log(resp);
        },
        rej => {
            this.setState({
                isFetching: false,
                resultsReceived: false,
                findingResult: undefined
            });
        });
    }

    render() {
        let fetching = this.state.isFetching;
        let resultsReceived = this.state.resultsReceived;

        return (
            <div className={s.identifyPage}>
                <IdentifyForm sendImage={this.sendImage}/>
                {
                    fetching || (!resultsReceived && this.state.fetched) ?
                        <Preloader play={fetching} success={resultsReceived}/>
                        :
                        null
                }

                {
                    resultsReceived ?
                        <Result results={this.state.findingResult}/>
                        :
                        null
                }
            </div>
        );
    }
}

export default IdentifyPage;