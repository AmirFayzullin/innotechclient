import React from 'react';
import s from './SliderStatusBar.module.css';

const SliderStatusBar = (props) => {
    let items = [];

    for(let i = 0; i < props.count; i++) {
        items.push(<div className={`${s.item} ${i === props.activeIndex ? s.active : ''}`}> </div>)
    }

    return (
        <div className={s.wrapper}>
            {items}
        </div>
    )
};

export default SliderStatusBar;