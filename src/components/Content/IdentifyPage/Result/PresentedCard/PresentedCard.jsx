import React from 'react';
import s from './PresentedCard.module.css';
import Preloader from "../../../../Preloader/Preloader";
import VkLink from "../../../../VkLink/VkLink";
import {URLReqConfig} from "../../../../../URLReqConfig";

class PresentedCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hidden: true,
            isFetchingAdditionalData: false,
            fetchingSuccess: false,
            additionalData: undefined
        };

        this.cardRef = React.createRef();

        this.exclusions = ['inn', 'name', 'vk_link'];

        this.compatibility = {
            "city": "Город",
            "info_direc": "Являлся учредителем",
            "info_staff": "Являлся сотрудником",
            "direct": "Являлся учредителем",
            "staff": "Являлся сотрудником",
        };

        this.onClose = this.onClose.bind(this);
        this.mapDataToJsx = this.mapDataToJsx.bind(this);
        this.mapAddDataToJsx = this.mapAddDataToJsx.bind(this);
        this.fetchAdditionalData = this.fetchAdditionalData.bind(this);
    }

    onClose(e) {
        let bound = this.cardRef.current.getBoundingClientRect();

        if (e.clientX < bound.right &&
            e.clientX > bound.left &&
            e.clientY < bound.bottom &&
            e.clientY > bound.top) {
            return;
        }

        this.setState({
            hidden: true
        });

        setTimeout(() => {
            this.props.resetPresentData();
        }, 300);
    };

    mapDataToJsx(data) {
        let content = [];
        if (data === undefined) return content;
        let dataKeys = Object.keys(data);

        let filteredKeys = dataKeys.filter(key => this.exclusions.indexOf(key) === -1);
        filteredKeys.sort();

        content.push(
            <React.Fragment>
                <header className={s.header}>
                    {data.name}
                </header>
                <div className={s.otherInfoItem}>
                    <p>INN</p>
                    <p>{data.inn}</p>
                </div>
            </React.Fragment>
        );

        filteredKeys.map((key, index) => {
            content.push(
                <div key={index} className={s.otherInfoItem}>
                    <p>{this.compatibility[key]}:</p>
                    <p>{data[key]}</p>
                </div>
            );

            return undefined;
        });

        return content;
    };

    mapAddDataToJsx(data) {
        if (!data) return;
        let content = [];
        let keys = Object.keys(data);

        keys.forEach((key) => {
            if (data[key] === undefined || data[key]?.length === 0) return;
            let box = [];

            box.push(
                <div className={s.title}>
                    <p>{this.compatibility[key]}:</p>
                </div>
            );

            data[key].map(d => {
                box.push(
                    <div className={s.otherInfoItem}>
                        <p>{d.name}</p>
                        <p>{d.raw_data}</p>
                    </div>
                );

                return undefined;
            });

            content.push(
                <div className={s.otherInfoItem + " " + s.otherInfoComplex}>
                    {box}
                </div>
            );
        });

        return content;
    }

    async fetchAdditionalData() {
        this.setState({
            isFetchingAdditionalData: true,
            fetchingSuccess: false
        });

        let reqBodyJson = JSON.stringify({
            // inn: toString(this.props.data.inn),
            inn: "773370633582"
        });

        fetch(`${URLReqConfig}/detail_user/`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            },
            body: reqBodyJson
        })
        .then(async response => {
            let resp = await response.json();
            console.log(resp);

            this.setState({
                isFetchingAdditionalData: false,
                fetchingSuccess: true,
                additionalData: resp
            });

            console.log(resp);
        },
        rej => {
            this.setState({
                isFetchingAdditionalData: false,
                fetchingSuccess: false,
                additionalData: undefined
            });
        });
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                hidden: false
            });
        }, 100);
        this.fetchAdditionalData();
    };

    render() {
        let fetchingNewData = this.state.isFetchingAdditionalData;
        let fetchingSuccess = this.state.fetchingSuccess;
        let content = this.mapDataToJsx(this.props.data);
        let additionalContent;

        if (fetchingSuccess) {
            additionalContent = this.mapAddDataToJsx(this.state.additionalData);
        }

        return (
            <div onClick={this.onClose} className={`${s.wrapper} ${this.state.hidden ? '' : s.opened}`}>
                <div ref={this.cardRef} className={s.card}>
                    {content}
                    {additionalContent}
                        <VkLink link={this.props.data.vk_link}/>

                    {
                        fetchingNewData || !fetchingSuccess ?
                            <Preloader play={fetchingNewData} success={fetchingSuccess}/>
                            :
                            null
                    }
                </div>
            </div>
        )
    }
}

export default PresentedCard;
