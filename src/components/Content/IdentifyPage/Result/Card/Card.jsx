import React from 'react';
import s from './Card.module.css';
import VkLink from "../../../../VkLink/VkLink";

const Card = (props) => {
    let data = props.data;

    let compatibility = {
        "city": "Город",
        "info_direc": "Являлся учредителем",
        "info_staff": "Являлся сотрудником"
    };

    let dataKeys = Object.keys(data);
    let exclusions = ['inn', 'name', 'vk_link'];

    let filteredKeys = dataKeys.filter(key => exclusions.indexOf(key) === -1);
    filteredKeys.sort();

    let otherInfo = filteredKeys.map((key, index) => (
        <div key={index} className={s.otherInfoItem}>
            <p>{compatibility[key]}:</p>
            <p>{data[key]}</p>
        </div>
    ));

    let onClick = () => {
        props.cardClick(props.index);
    };

    return (
        <div className={s.cardWrapper}>
            <div onClick={onClick} className={s.card}>
                <header className={s.header}>{data.name}</header>
                <div className={s.otherInfoItem}>
                    <p>ИНН</p>
                    <p>{data.inn}</p>
                </div>
                {otherInfo}
            </div>
            <VkLink link={data.vk_link}/>
        </div>
    );
};

export default Card;