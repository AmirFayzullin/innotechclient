import React from 'react';
import s from './Result.module.css';
import Card from "./Card/Card";
import PresentedCard from "./PresentedCard/PresentedCard";

class Result extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inView: 0,
            hidden: true,
            presentData: undefined
        };

        this.resultsLength = 0;

        this.onDec = this.onDec.bind(this);
        this.onInc = this.onInc.bind(this);
        this.setPresentCardData = this.setPresentCardData.bind(this);
        this.resetPresentData = this.resetPresentData.bind(this);
    }

    onDec() {
        this.setState((state) => ({
            inView: state.inView > 1 ? state.inView - 1 : 0,
        }));
    }

    onInc() {
        this.setState((state) => ({
            inView: state.inView < this.resultsLength - 1 ? state.inView + 1 : state.inView,
        }));
    }

    setPresentCardData(index) {
        this.setState({
            presentData: this.props.results[index],
        });
    }

    resetPresentData() {
        this.setState({
            presentData: undefined
        });
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                hidden: false
            });
        }, 100);
        this.resultsLength = this.props.results?.length
    }

    render() {
        console.log(this.props.results, this.state.presentData);
        let cards = this.props.results?.map((data, index) => (
            <Card cardClick={this.setPresentCardData}
                  index={index}
                  key={index}
                  data={data}
            />
            )
        );

        return (
            <React.Fragment>
                <div className={`${s.wrapper} ${this.state.hidden ? '' : s.displayed}`}>
                    <header className={s.header}>
                        <p>Результат поиска</p>
                        {/*<SliderStatusBar count={cardsDataPacks.length}*/}
                        {/*             activeIndex={this.state.inView}*/}
                        {/*/>*/}

                    </header>
                    {/*<div className={s.back} onClick={this.onDec}>
                        back
                    </div>
                    <div className={s.next} onClick={this.onInc}>
                        next
                    </div>*/}
                    <div className={s.cards}>
                        {cards}
                    </div>
                </div>

                {this.state.presentData !== undefined ?
                    <PresentedCard resetPresentData={this.resetPresentData}
                                   data={this.state.presentData}
                    />
                    :
                    null
                }
            </React.Fragment>
        )
    }
}

export default Result;