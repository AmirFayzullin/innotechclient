import React from 'react';
import s from './FindingResult.module.css';
import PersonCard from "./PersonCard/PersonCard";

class FindingResult extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hidden: true
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                hidden: false
            });
        }, 100);
    }

    render() {
        let results = [
            {
                name: "Sue",
            },
            {
                name: "John",
            },
        ];

        let cards = results.map((data, index) => <PersonCard key={index} data={data} />);

        return (
            <div className={`${s.findingResult} ${this.state.hidden ? '' : s.displayed}`}>
                <header>
                    Results
                </header>
                <div className={s.cards}>
                    {cards}
                </div>
            </div>
        );
    }
}


export default FindingResult;