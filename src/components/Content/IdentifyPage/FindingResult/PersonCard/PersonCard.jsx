import React from 'react';
import s from './PersonCard.module.css';

const PersonCard = (props) => {
    let data = props.data;

    return (
        <div className={s.wrapper}>
            {data.name}
        </div>
    )
};

export default PersonCard;