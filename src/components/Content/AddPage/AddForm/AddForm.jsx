import React from 'react';
import s from './AddForm.module.css';

class AddForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filePath: undefined,
            imageBlob: undefined,
            vkId: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onChangeVkId = this.onChangeVkId.bind(this);
        this.onSend = this.onSend.bind(this);
    }

    onChange(e) {
        let input = e.target;

        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = ((e) => {
                this.setState({
                    filePath: e.target.result,
                    imageBlob: input.files[0]
                });
            });

            reader.readAsDataURL(input.files[0]);
        }
    }

    onChangeVkId(e) {
        this.setState({
            vkId: e.target.value
        });
    }

    onSend(e) {
        e.preventDefault();

        if (!this.state.vkId.length) return;

        let formData = new FormData();

        formData.append("vk_id", this.state.vkId);

        this.props.sendImage(formData);
    }

    render() {
        return (
            <div className={s.identifyFormWrapper}>

                <form className={s.form}>

                    <label className={s.inputLabel}>
                        VK id:
                        <input className={`${s.input} ${this.state.vkId ? s.opened : ''}`} onChange={this.onChangeVkId} value={this.state.vkId}/>
                    </label>

                    <label className={`${s.formItem} ${this.state.vkId ? s.active : s.inactive}`}>
                        <button className={`${s.sendButton} ${this.state.vkId ? s.active : s.inactive}`} onClick={this.onSend}>Дообучить</button>
                    </label>
                </form>
            </div>
        )
    }
}

export default AddForm;