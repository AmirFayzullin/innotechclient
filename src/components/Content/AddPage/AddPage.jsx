import React from 'react';
import s from '../IdentifyPage/IdentifyPage.module.css';
import Preloader from "../../Preloader/Preloader";
import AddForm from "./AddForm/AddForm";
import {URLReqConfig} from "../../../URLReqConfig";

class AddPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            responseReceived: false,
            success: false
        };

        this.sendImage = this.sendImage.bind(this);
    }

    async sendImage(formData) {
        this.setState({
            isFetching: true,
            responseReceived: false,
        });

        let isOk;

        fetch(`${URLReqConfig}/add_user_by_id/`, {
            method: 'POST',
            body: formData
        })
        .then(async response => {
            isOk = (await response).status === 200;

            this.setState({
                isFetching: false,
                responseReceived: true,
                success: isOk
            });

            console.log(isOk);
        },
        rej => {
            this.setState({
                isFetching: false,
                responseReceived: true,
                success: false
            });
        });

        /*setTimeout(() => {
            this.setState({
                isFetching: false,
                responseGot: true,
                findingResult: resp
            });
        }, 2000);*/
    }

    render() {
        let fetching = this.state.isFetching;
        let responseReceived = this.state.responseReceived;
        let success = this.state.success;

        return (
            <div className={s.identifyPage}>
                <AddForm sendImage={this.sendImage} />
                {
                    responseReceived || fetching ?
                        <Preloader play={fetching} success={success}/>
                        :
                        null
                }
            </div>
        );
    }
}

export default AddPage;