import React from 'react';
import {Route} from "react-router-dom";
import IdentifyPage from "./IdentifyPage/IdentifyPage";
import AddPage from "./AddPage/AddPage";
import s from './Content.module.css';

const Content = () => {
    return (
        <div className={s.contentPage}>
            <Route path='/' component={IdentifyPage} exact/>
            <Route path='/add' component={AddPage}/>
        </div>
    )
};

export default Content;