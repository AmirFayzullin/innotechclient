import React from 'react';
import s from './Preloader.module.css';

class Preloader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hidden: false,
            waitingHiddenMode: false
        }
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (!this.state.hidden &&
            !this.state.waitingHiddenMode &&
            this.props.play &&
            !nextProps.play) {

            this.setState({
               waitingHiddenMode: true
            });

            setTimeout(() => {
                this.setState({
                   hidden: true,
                   waitingHiddenMode: false
                });
            }, 2000);
        } else if (this.state.hidden &&
            !this.state.waitingHiddenMode &&
            !this.props.play &&
            nextProps.play) {

            this.setState({
                hidden: false
            });
        }

        return true;
    }

    render() {
        let status;
        if (this.props.play) {
            status = <div className={s.preloader}> </div>;
        } else {
            if (this.props.success) {
                status = <div className={s.success}> </div>;
            } else {
                status = <div className={s.fail}> </div>;
            }
        }

        return (
            <div className={s.wrapper}>
                <div className={`${s.statusWrapper} ${this.state.hidden ? s.hidden : ''}`}>
                    {status}
                </div>
            </div>
        );
    }
}

export default Preloader;