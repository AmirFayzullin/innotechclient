import React from 'react';
import s from './VkLink.module.css';

const VkLink = (props) => {

    let redirectToSocial = () => {
        window.location.href = props.link;
    };

    return (
        <React.Fragment>
            {
                props.link !== undefined ?
                    <div onClick={redirectToSocial} className={s.linkWrapper}>
                        <img className={s.linkLogo} src={`${process.env.PUBLIC_URL}/vkLogo.svg`} alt=""/>
                    </div>
                    :
                    null
            }
        </React.Fragment>
    )
};

export default VkLink;