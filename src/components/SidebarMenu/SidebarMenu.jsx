import React from 'react';
import {NavLink} from "react-router-dom";
import s from './SidebarMenu.module.css';

const SidebarMenu = () => {
  return (
      <div className={s.sidebar}>
          <NavLink exact to='/' activeClassName={s.active}>Найти по фото</NavLink>
          <NavLink to='/add' activeClassName={s.active}>Дообучить</NavLink>
      </div>
  );
};

export default SidebarMenu;