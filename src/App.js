import React from 'react';
import './App.css';
import SidebarMenu from "./components/SidebarMenu/SidebarMenu";
import Content from "./components/Content/Content";

const App = () => {
  return (
      <div className='appWrapper'>
        <SidebarMenu/>
        <Content />
      </div>
  );
};

export default App;
